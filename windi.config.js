export default {
    plugins: [
        require('windicss/plugin/typography'),
    ],
    theme: {
        extend: {
            typography: {
                DEFAULT: {
                    css: {
                        color: 'inherit',
                        'h1': {
                            color: 'inherit',
                        },
                        'h2': {
                            color: 'inherit',
                        },
                        'h3': {
                            color: 'inherit',
                        },
                        'blockquote': {
                            color: 'inherit',
                        },
                        'code': {
                            color: 'inherit',
                        },
                        'strong': {
                            color: 'inherit',
                        },},
                },
            },
        },
    },
}